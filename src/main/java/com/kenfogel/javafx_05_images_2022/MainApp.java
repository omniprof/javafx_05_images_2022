package com.kenfogel.javafx_05_images_2022;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * Example of displaying an image
 *
 * @author Ken
 */
public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Teacher Flying");

        // Photo is 4 x 3 so I calculated the correct ratio
        int width = 800;
        int height = 600;
        String path = "/images/FreeFall.jpg";

        Group root = new Group();

        root.getChildren().add(createImageView(path, width, height));
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    /**
     * Create an ImageView that scales the image
     *
     * @param path
     * @param width
     * @param height
     * @return
     */
    private ImageView createImageView(String path, int width, int height) {
        
        Image freeFall = new Image(
                getClass().getResourceAsStream(path));
        ImageView imageView = new ImageView();
        imageView.setImage(freeFall);
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
        return imageView;
    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
